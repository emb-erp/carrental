package com.emb.carrental.util;

import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author bu_000
 */
public class DatabaseIT {

    private List<String> distances;

    @Before
    public void initClient() {
        distances = new ArrayList<>();
        distances.add("Pune-Mumbai"); // distance = 200.0 KM
        distances.add("Mumbai-Bangalore");  // distance = 400.0 KM
    }

    @Test
    public void calculateTotalDistance() throws InvalidDistanceException {
        double totalDistance = Database.calculateTotalDistance(distances);
        assertThat(totalDistance, is(600.0));
        distances.add("Bangalore-Pune"); // distance = 1000.0 KM
        totalDistance = Database.calculateTotalDistance(distances);
        assertThat(totalDistance, is(1600.0));
        LOG.info(String.valueOf(totalDistance));
    }
}
