package com.emb.carrental.app.control;

import com.emb.carrental.app.model.Bus;
import com.emb.carrental.app.model.Car;
import com.emb.carrental.app.model.SUV;
import com.emb.carrental.app.model.Swift;
import com.emb.carrental.app.model.Truck;
import com.emb.carrental.app.model.Van;
import com.emb.carrental.util.Constants;
import com.emb.carrental.util.InvalidDistanceException;
import com.emb.carrental.util.Trip;

/**
 *
 * @author bu_000
 */
public class RentalService {

    public double calculateTotalExpenseForTrip(Trip trip) throws InvalidDistanceException {
        switch (trip.getVehicleType()) {
            case Constants.BUS:
                return new Bus().calculateTotalExpenseForTrip(trip);
            case Constants.CAR:
                return new Car().calculateTotalExpenseForTrip(trip);
            case Constants.SUV:
                return new SUV().calculateTotalExpenseForTrip(trip);
            case Constants.SWIFT:
                return new Swift().calculateTotalExpenseForTrip(trip);
            case Constants.VAN:
                return new Van().calculateTotalExpenseForTrip(trip);
            case Constants.TRUCK:
                return new Truck().calculateTotalExpenseForTrip(trip);
            default:
                return 0;
        }
    }

}
