package com.emb.carrental.util;

/**
 *
 * @author bu_000
 */
public class Constants {

    public static final String PETROL = "Petrol";
    public static final String DIESEL = "Diesel";
    public static final String AC = "AC";
    public static final String BUS = "Bus";
    public static final String CAR = "Car";
    public static final String SUV = "SUV";
    public static final String SWIFT = "Swift";
    public static final String VAN = "Van";
    public static final String TRUCK = "Truck";
    public static final int DIGITS = 2;
    public static final double STANDARD_RATE = 15;
    public static final int DIESEL_ADJUSTMENT = 1;
    public static final int ADDITIONAL_RATE = 2;
    public static final int ADDITIONAL_CHARGE_RATE_PER_KM_AND_PERSON = 1;
}
