package com.emb.carrental.app.model;

import com.emb.carrental.util.Constants;
import com.emb.carrental.util.Database;
import com.emb.carrental.util.InvalidDistanceException;
import com.emb.carrental.util.Trip;
import com.emb.carrental.util.Util;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bu_000
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Bus extends Vehicle{

    private Trip trip;
    private double discountRate = 0.02;

    public double getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(double discountRate) {
        this.discountRate = discountRate;
    }

    /**
     *
     * @param trip
     * @return
     * @throws InvalidDistanceException Buses receive 2% discount on standard
     * Rate
     */
    @Override
    public double calculateTotalExpenseForTrip(Trip trip)
            throws InvalidDistanceException {
        this.type = trip.getType();
        this.noOfPassengers = trip.getNoOfPassengers();
        rate = new Rate(trip.getEngineType(), isAC());
        double standardRate = rate.getStandardRate();
        rate.setStandardRate(standardRate - (standardRate * discountRate));
        double totalDistance = Database.calculateTotalDistance(trip.getDistances());
        double additionalCharge = 0;

        if (limitIsExceeded()) {
            additionalCharge = Constants.ADDITIONAL_CHARGE_RATE_PER_KM_AND_PERSON
                    * totalDistance * noOfPassengers;
        }
        double totalExpenseForTrip = (rate.getStandardRate()
                * totalDistance) + additionalCharge;
        return Util.roundOff(totalExpenseForTrip, Constants.DIGITS);
    }

    /**
     *
     * @return @throws InvalidDistanceException Buses receive 2% discount on
     * standard Rate
     */
    @Override
    public double calculateTotalExpenseForTrip() throws InvalidDistanceException {
        if (trip == null) {
            throw new InvalidDistanceException("Trip is required");
        }
        return calculateTotalExpenseForTrip(trip);
    }
}
